package controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import clases.Jugador;

@RestController
public class JugadorController {
   @Autowired
   JdbcTemplate template;

   @GetMapping
   public List<Jugador> ObtenerJugadores(){
       List<Jugador> res = new ArrayList();
      String sql = "SELECT * FROM JUGADOR";
      SqlRowSet rs = template.queryForRowSet(sql);
      while(rs.next()){
         res.add(ObtenerRegistro(rs));
      }
      return res;  
   }

   @PostMapping
   public void agregarJugador(@RequestBody Jugador jugador){
      String sql = "INSERT INTO jugador(CODIGO, DNI, NOMBRE, FECHA_NACIMIENTO,PESO,TALLA,FECHA_INICIO,CLUB";
        template.update(sql, jugador.getCodigo(), jugador.getDNI(), jugador.getNombre(), jugador.getFecha_nacimiento(), jugador.getPeso(), jugador.getTalla(), jugador.getFecha_inicio(), jugador.getClub() );
    }

   }

   @PatchMapping
   public void actualizarJugador(@RequestBody Jugador jugador){
   
   }

   @DeleteMapping
   public void borrarJugador(@RequestBody Jugador jugador){
      
   }
   private Jugador ObtenerRegistro(SqlRowSet rs){
      Jugador jugador = new Jugador();
      jugador.setCodigo(rs.getString("CODIGO"));
      jugador.setNombre(rs.getString("NOMBRE"));
      jugador.setDNI(rs.getString("DNI"));
      jugador.setFecha_nacimiento(rs.getDate("FECHA_NACIMIENTO").toLocalDate());
      jugador.setPeso(rs.getBigDecimal("PESO"));
      jugador.setTalla(rs.getBigDecimal("TALLA"));
      jugador.setFecha_inicio(rs.getDate("fECHA_INICIO").toLocalDate());
      jugador.setClub(rs.getString("CLUB"));
      return jugador;
    
  }

}
