package CreacionTablas;

import java.sql.*;

public class CreacionTablas {
    public static void Creacion_Cliente(String DNI,String Nombre,String Apellido,String Correo,String Telefono,String Alergias) throws Exception{
        Class.forName("org.postgresql.Driver");
        String url="jdbc:postgresql://localhost:5432/postgres";
        String user="postgres";
        Connection conn= DriverManager.getConnection(url, user, "");
        PreparedStatement pst;
        String sql="INSERT INTO USUARIO VALUES(?,?,?,?,?,?)";
        pst=conn.prepareStatement(sql);
        pst.setString(1,DNI);
        pst.setString(2,Nombre);
        pst.setString(3,Apellido);
        pst.setString(4,Correo);
        pst.setString(5,Telefono);
        pst.setString(6,Alergias);


        pst.executeUpdate();

        pst.close();
        conn.close();
    }
    public static void Creacion_Aerolines(String Nombre,String Pagina_Web,String Direccion,String Correo,String Telefono) throws Exception{
        Class.forName("org.postgresql.Driver");
        String url="jdbc:postgresql://localhost:5432/postgres";
        String user="postgres";
        Connection conn= DriverManager.getConnection(url, user, "");
        PreparedStatement pst;
        String sql="INSERT INTO USUARIO VALUES(?,?,?,?,?)";
        pst=conn.prepareStatement(sql);
        pst.setString(1,Nombre);
        pst.setString(2,Pagina_Web);
        pst.setString(3,Direccion);
        pst.setString(4,Correo);
        pst.setString(5,Telefono);


        pst.executeUpdate();

        pst.close();
        conn.close();
    }
    public static void Creacion_Aeronove(String Codigo,String Fabricante,String Modelo,int Capacidad,String Descripcion,Date Fecha_Compra) throws Exception{
        Class.forName("org.postgresql.Driver");
        String url="jdbc:postgresql://localhost:5432/postgres";
        String user="postgres";
        Connection conn= DriverManager.getConnection(url, user, "");
        PreparedStatement pst;
        String sql="INSERT INTO USUARIO VALUES(?,?,?,?,?,?)";
        pst=conn.prepareStatement(sql);
        pst.setString(1,Codigo);
        pst.setString(2,Fabricante);
        pst.setString(3,Modelo);
        pst.setInt(4,Capacidad);
        pst.setString(5,Descripcion);
        pst.setDate(6,Fecha_Compra);


        pst.executeUpdate();

        pst.close();
        conn.close();
    }

}
