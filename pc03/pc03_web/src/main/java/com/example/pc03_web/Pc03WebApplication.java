package com.example.pc03_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pc03WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pc03WebApplication.class, args);
	}

}
